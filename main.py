import pandas as pd
import openai
import requests
from instagrapi.exceptions import LoginRequired, PleaseWaitFewMinutes
import time
from instagrapi import Client
from matplotlib import pyplot as plt
import csv
import numpy as np
import datetime

ACCOUNT_USERNAME = "notanactualbot6"
ACCOUNT_PASSWORD = "notanactualbot6*"
openai.api_key = "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"
input_file = "data.csv"
fieldnames = ["Date", "LikeCount", "Comment"]
fieldnames2 = ["Date", "ID", "LikeCount", "Comment"]
dict_list = []
# !!!!!! too many (medias/ posts') comments may result in the api overloading !!!!  - fixed by printing the error??
# !!!!!!! too many login requests results in an account 1-2 days timeout or possible ban !!!!!!!!!!
user_id = '1903424587'
try:
    cl = Client()
    user_id = cl.user_id_from_username('letourdefrance')
except requests.exceptions.RetryError as e:
    print("Didn't succeed connecting to the user!")


def get_post_comments():
    print("Obtaining media...")
    cl.login(ACCOUNT_USERNAME, ACCOUNT_PASSWORD)
    medias = cl.user_medias(user_id, 5)
    print("Obtaining comments...")
    try:
        for el in medias:
            temp = cl.media_comments(el.id)
            for com in temp:
                d = {}
                d["Date"] = el.taken_at.date().isoformat()
                if com.like_count is None:
                    d["LikeCount"] = 0
                else:
                    d["LikeCount"] = com.like_count
                d["Comment"] = com.text
                dict_list.append(d)
    except (LoginRequired, PleaseWaitFewMinutes):
        cl.relogin()
    print("Finished collecting comments!")


def analyze_comment(comment):
    retries = 100
    sentiment = None

    while retries > 0:
        messages = [
            {"role": "system",
             "content": "You are an AI language model trained to analyze and detect the sentiment of a comment on a post."},
            {"role": "user",
             "content": f"Analyze the following comments and determine if the sentiment is: positive, negative or "
                        f"neutral. Return only a single word, either POSITIVE, NEGATIVE or NEUTRAL: {comment}"}
        ]

        try:
            completion = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=messages,
                # We only want a single word sentiment determination, so we limit the results to 3 openAI tokens,
                # which is about 1 word. If you set a higher max_tokens amount, openAI will generate a bunch of
                # additional text for each response, which is not what we want it to do
                max_tokens=3,
                n=1,
                stop=None,
                temperature=0
            )

            response_text = completion.choices[0].message.content
            # print the sentiment for each customer review, not necessary, but it's nice to see the API doing something :)
            print(response_text + ":\t" + comment)

            # Sometimes, the API will be overwhelmed or just buggy, so we need to check if the response from the API was
            # an error message or one of our allowed sentiment classifications. If the API returns something other than
            # POSITIVE, NEGATIVE or NEUTRAL, we will retry that particular review that had a problem up to 3 times. This
            # is usually enough.
            if response_text in ["POSITIVE", "NEGATIVE", "NEUTRAL"]:
                sentiment = response_text
                break
            else:
                retries -= 1
                time.sleep(0.5)
        except openai.error.OpenAIError as e:
            print(e)
    else:
        sentiment = "neutral"

    # OpenAI will limit the number of times you can access their API if you have a free account. If you are using the
    # openAI free tier, you need to add a delay of a few seconds (i.e. 4 seconds) between API requests to avoid
    # hitting the openai free tier API call rate limit. This code will still work with an openAI free tier account,
    # but you should limit the number of comments you want to analyze (<100 at a time) to avoid running into random
    # API problems.
    # time.sleep(1)
    return sentiment


def write_to_csv():
    print("Writing to CSV...")
    with open("comments.csv", mode="w", newline='', encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in dict_list:
            writer.writerow(row)

    df = pd.read_csv("comments.csv", skip_blank_lines=True)
    sentiments = []
    for comment in df["Comment"]:
        sentiment = analyze_comment(comment)
        sentiments.append(sentiment)

    df["Sentiment"] = sentiments
    print("Adding the sentiment for each comment...")
    df.to_csv('comments.csv', index=False)
    combine_csv("data.csv", "comments.csv")
    print("Finished writing to CSV!")


def combine_csv(file1, file2):
    file1 = pd.read_csv(file1)
    file2 = pd.read_csv(file2)
    file1 = file1.merge(file2, how='outer')
    file1.sort_values(["Date"],
                      axis=0,
                      ascending=[True],
                      inplace=True)
    file1.to_csv("data.csv", index=False)


def plot_opinion():
    df = pd.read_csv("data.csv", skip_blank_lines=True)
    positive = []
    negative = []

    # group dates & positive/ negative  like_count per day
    dates = [*set(df["Date"])]
    dates = sorted(dates, key=lambda date: datetime.datetime.strptime(date, '%Y-%m-%d'))
    values = np.arange(len(dates))
    for i in dates:
        positive.append(sum(df['LikeCount'][df['Sentiment'] == 'POSITIVE'][df['Date'] == i].values + 1))
        negative.append(sum(df['LikeCount'][df['Sentiment'] == 'NEGATIVE'][df['Date'] == i].values + 1))
    plt.bar(values - 0.1, positive, label='Positive', color='blue', width=0.2)
    plt.bar(values + 0.1, negative, label='Negative', color='red', width=0.2)
    plt.title("Public opinion")
    plt.xlabel("Date")
    plt.ylabel("Like count")
    plt.legend()
    plt.xticks(values, dates, fontsize=4)
    plt.savefig("public_opinion.png")
    plt.show()


def plot_player_performance():
    players = ["competitors/CHRISTOPHE LAPORTE.csv", "competitors/JONAS VINGEGAARD.csv", "competitors/TIESJ BENOOT.csv",
               "competitors/WILCO KELDERMAN.csv", "competitors/SEPP KUSS.csv", "competitors/WOUT VAN AERT.csv",
               "competitors/NATHAN VAN HOOYDONCK.csv", "competitors/DYLAN VAN BAARLE.csv"]

    f = plt.figure(1)
    plt.xlim([0, 22])
    plt.ylim([0, 168])
    plt.xticks(np.arange(0, 23))
    plt.xlabel('Stage')
    plt.ylabel('Stage rank')
    plt.title("Player rankings per stage", color='black')
    for player in players:
        col = (np.random.random(), np.random.random(), np.random.random())
        plt.rcParams['text.color'] = col
        df = pd.read_csv(player, skip_blank_lines=True)
        plt.plot(df["Stage"], df["StageRank"], label=f'{player} stage rank', color=col)
        plt.annotate(f'{player.replace("competitors/", "").replace(".csv", "")}',
                     xy=(df["Stage"][len(df["Stage"]) - 1], df["StageRank"][len(df["StageRank"]) - 1]))
    plt.savefig("stage_rankings.png")
    f.gca().invert_yaxis()
    f.show()

    plt.figure(2)
    plt.xlim([0, 22])
    plt.ylim([0, 168])
    plt.xlabel('Stage')
    plt.ylabel('Final rank')
    plt.title("Player rankings", color='black')
    plt.xticks(np.arange(0, 23))
    for player in players:
        col = (np.random.random(), np.random.random(), np.random.random())
        plt.rcParams['text.color'] = col
        df = pd.read_csv(player, skip_blank_lines=True)
        plt.plot(df["Stage"], df["OverallRank"], label=f'{player} overall rank', color=col)
        plt.annotate(f'{player.replace("competitors/", "").replace(".csv", "")}',
                     xy=(df["Stage"][len(df["Stage"]) - 1], df["OverallRank"][len(df["OverallRank"]) - 1]))
    plt.savefig("overall_rankings.png")
    plt.gca().invert_yaxis()
    plt.show()


get_post_comments()
write_to_csv()
plot_opinion()
plot_player_performance()
